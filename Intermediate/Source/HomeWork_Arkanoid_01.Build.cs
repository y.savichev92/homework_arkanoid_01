using UnrealBuildTool;

public class HomeWork_Arkanoid_01 : ModuleRules
{
	public HomeWork_Arkanoid_01(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PrivateDependencyModuleNames.Add("Core");
		PrivateDependencyModuleNames.Add("Core");
	}
}
