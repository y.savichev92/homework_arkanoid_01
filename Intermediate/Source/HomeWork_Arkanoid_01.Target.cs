using UnrealBuildTool;

public class HomeWork_Arkanoid_01Target : TargetRules
{
	public HomeWork_Arkanoid_01Target(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("HomeWork_Arkanoid_01");
	}
}
